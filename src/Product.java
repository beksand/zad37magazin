public class Product {
    private String nazwa;
    private PRODUCT_TYPE type;
    private PRODUCT_CLASS polka;
    private double cena;

    public Product(String nazwa, PRODUCT_TYPE type, PRODUCT_CLASS polka, double cena) {
        this.nazwa = nazwa;
        this.type = type;
        this.polka = polka;
        this.cena = cena;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setType(PRODUCT_TYPE type) {
        this.type = type;
    }

    public PRODUCT_CLASS getPolka() {
        return polka;
    }

    public void setPolka(PRODUCT_CLASS polka) {
        this.polka = polka;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Product{" +
                "nazwa='" + nazwa + '\'' +
                ", type=" + type +
                ", polka=" + polka +
                ", cena=" + cena +
                '}';
    }
}
