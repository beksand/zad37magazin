import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazine {

    Map<PRODUCT_CLASS, List<Product>> sklep = new HashMap<>();

    public Magazine() {
    }

    public Map<PRODUCT_CLASS, List<Product>> getSklep() {
        return sklep;
    }

    public void setSklep(Map<PRODUCT_CLASS, List<Product>> sklep) {
        this.sklep = sklep;
    }

    public Magazine(Map<PRODUCT_CLASS, List<Product>> sklep) {
        this.sklep = new HashMap<>();
    }

    public void addProduct(PRODUCT_CLASS type, List<Product> products){
        sklep.put(type, products);
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "sklep=" + sklep +
                '}';
    }
}
